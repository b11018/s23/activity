// alert(`s23 activity`)

function Winchester(name, level){

	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = level;

	this.flick = function(target){
		console.log(`${this.name} flicks ${target.name} on the head and irritates him`)
		console.log(`${target.name}'s health is now reduced to ${target.health - (this.atk - 7)}`)
		target.health = target.health - (this.atk - 7)
		if (target.health < 10){
			target.faint()
		};
	}

	this.kick = function(target){
		console.log(`${this.name} takes a step back and kicks ${target.name} on his shin`)
		console.log(`${target.name}'s health is now reduced to ${(target.health) - (this.atk - 5)}`)
		target.health = target.health - (this.atk - 5)
		if (target.health < 10){
			target.faint()
		};
	}

	this.punch = function(target){
		console.log(`${this.name} grabs ${target.name}'s shirt and punches his face`)
		console.log(`${target.name}'s health is now reduced to ${(target.health) - (this.atk - 2)}`)
		target.health = target.health - (this.atk - 2)
		if (target.health < 10){
			target.faint()
		};
	};

	this.faint = function(){
				console.log(`${this.name} fainted`)
			}
};

let dean = new Winchester("Dean", 10);
let sam = new Winchester("Sam", 9);

console.log(dean);
console.log(sam);

dean.flick(sam);
sam.kick(dean);
dean.kick(sam)
sam.punch(dean);